/**
 */
package neuralNetwork;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link neuralNetwork.DataSet#getDatapoint <em>Datapoint</em>}</li>
 *   <li>{@link neuralNetwork.DataSet#getName <em>Name</em>}</li>
 *   <li>{@link neuralNetwork.DataSet#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see neuralNetwork.NeuralNetworkPackage#getDataSet()
 * @model
 * @generated
 */
public interface DataSet extends EObject {
	/**
	 * Returns the value of the '<em><b>Datapoint</b></em>' containment reference list.
	 * The list contents are of type {@link neuralNetwork.DataPoint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Datapoint</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Datapoint</em>' containment reference list.
	 * @see neuralNetwork.NeuralNetworkPackage#getDataSet_Datapoint()
	 * @model containment="true"
	 * @generated
	 */
	EList<DataPoint> getDatapoint();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see neuralNetwork.NeuralNetworkPackage#getDataSet_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link neuralNetwork.DataSet#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link neuralNetwork.DatasetType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see neuralNetwork.DatasetType
	 * @see #setType(DatasetType)
	 * @see neuralNetwork.NeuralNetworkPackage#getDataSet_Type()
	 * @model
	 * @generated
	 */
	DatasetType getType();

	/**
	 * Sets the value of the '{@link neuralNetwork.DataSet#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see neuralNetwork.DatasetType
	 * @see #getType()
	 * @generated
	 */
	void setType(DatasetType value);

} // DataSet
