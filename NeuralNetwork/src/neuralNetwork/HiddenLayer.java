/**
 */
package neuralNetwork;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hidden Layer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link neuralNetwork.HiddenLayer#getNeurons <em>Neurons</em>}</li>
 * </ul>
 *
 * @see neuralNetwork.NeuralNetworkPackage#getHiddenLayer()
 * @model
 * @generated
 */
public interface HiddenLayer extends EObject {
	/**
	 * Returns the value of the '<em><b>Neurons</b></em>' containment reference list.
	 * The list contents are of type {@link neuralNetwork.Neuron}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Neurons</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Neurons</em>' containment reference list.
	 * @see neuralNetwork.NeuralNetworkPackage#getHiddenLayer_Neurons()
	 * @model containment="true" required="true" upper="8"
	 * @generated
	 */
	EList<Neuron> getNeurons();

} // HiddenLayer
