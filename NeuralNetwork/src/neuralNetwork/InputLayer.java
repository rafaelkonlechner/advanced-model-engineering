/**
 */
package neuralNetwork;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Input Layer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link neuralNetwork.InputLayer#getPerceptrons <em>Perceptrons</em>}</li>
 * </ul>
 *
 * @see neuralNetwork.NeuralNetworkPackage#getInputLayer()
 * @model
 * @generated
 */
public interface InputLayer extends EObject {
	/**
	 * Returns the value of the '<em><b>Perceptrons</b></em>' containment reference list.
	 * The list contents are of type {@link neuralNetwork.Perceptron}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Perceptrons</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Perceptrons</em>' containment reference list.
	 * @see neuralNetwork.NeuralNetworkPackage#getInputLayer_Perceptrons()
	 * @model containment="true" upper="4"
	 * @generated
	 */
	EList<Perceptron> getPerceptrons();

} // InputLayer
