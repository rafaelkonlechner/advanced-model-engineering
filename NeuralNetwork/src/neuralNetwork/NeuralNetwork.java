/**
 */
package neuralNetwork;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Neural Network</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link neuralNetwork.NeuralNetwork#getActivationFunction <em>Activation Function</em>}</li>
 *   <li>{@link neuralNetwork.NeuralNetwork#getInputlayer <em>Inputlayer</em>}</li>
 *   <li>{@link neuralNetwork.NeuralNetwork#getHiddenlayer <em>Hiddenlayer</em>}</li>
 *   <li>{@link neuralNetwork.NeuralNetwork#getDataset <em>Dataset</em>}</li>
 * </ul>
 *
 * @see neuralNetwork.NeuralNetworkPackage#getNeuralNetwork()
 * @model
 * @generated
 */
public interface NeuralNetwork extends EObject {
	/**
	 * Returns the value of the '<em><b>Activation Function</b></em>' attribute.
	 * The literals are from the enumeration {@link neuralNetwork.ActivationFunction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Activation Function</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Activation Function</em>' attribute.
	 * @see neuralNetwork.ActivationFunction
	 * @see #setActivationFunction(ActivationFunction)
	 * @see neuralNetwork.NeuralNetworkPackage#getNeuralNetwork_ActivationFunction()
	 * @model
	 * @generated
	 */
	ActivationFunction getActivationFunction();

	/**
	 * Sets the value of the '{@link neuralNetwork.NeuralNetwork#getActivationFunction <em>Activation Function</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Activation Function</em>' attribute.
	 * @see neuralNetwork.ActivationFunction
	 * @see #getActivationFunction()
	 * @generated
	 */
	void setActivationFunction(ActivationFunction value);

	/**
	 * Returns the value of the '<em><b>Inputlayer</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inputlayer</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inputlayer</em>' containment reference.
	 * @see #setInputlayer(InputLayer)
	 * @see neuralNetwork.NeuralNetworkPackage#getNeuralNetwork_Inputlayer()
	 * @model containment="true" required="true"
	 * @generated
	 */
	InputLayer getInputlayer();

	/**
	 * Sets the value of the '{@link neuralNetwork.NeuralNetwork#getInputlayer <em>Inputlayer</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inputlayer</em>' containment reference.
	 * @see #getInputlayer()
	 * @generated
	 */
	void setInputlayer(InputLayer value);

	/**
	 * Returns the value of the '<em><b>Hiddenlayer</b></em>' containment reference list.
	 * The list contents are of type {@link neuralNetwork.HiddenLayer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hiddenlayer</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hiddenlayer</em>' containment reference list.
	 * @see neuralNetwork.NeuralNetworkPackage#getNeuralNetwork_Hiddenlayer()
	 * @model containment="true" upper="4"
	 * @generated
	 */
	EList<HiddenLayer> getHiddenlayer();

	/**
	 * Returns the value of the '<em><b>Dataset</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dataset</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dataset</em>' containment reference.
	 * @see #setDataset(DataSet)
	 * @see neuralNetwork.NeuralNetworkPackage#getNeuralNetwork_Dataset()
	 * @model containment="true" required="true"
	 * @generated
	 */
	DataSet getDataset();

	/**
	 * Sets the value of the '{@link neuralNetwork.NeuralNetwork#getDataset <em>Dataset</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dataset</em>' containment reference.
	 * @see #getDataset()
	 * @generated
	 */
	void setDataset(DataSet value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void test(DataSet dataset);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void train(DataSet dataset);

} // NeuralNetwork
