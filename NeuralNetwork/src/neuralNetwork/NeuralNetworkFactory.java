/**
 */
package neuralNetwork;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see neuralNetwork.NeuralNetworkPackage
 * @generated
 */
public interface NeuralNetworkFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	NeuralNetworkFactory eINSTANCE = neuralNetwork.impl.NeuralNetworkFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Neural Network</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Neural Network</em>'.
	 * @generated
	 */
	NeuralNetwork createNeuralNetwork();

	/**
	 * Returns a new object of class '<em>Data Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Set</em>'.
	 * @generated
	 */
	DataSet createDataSet();

	/**
	 * Returns a new object of class '<em>Data Point</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Point</em>'.
	 * @generated
	 */
	DataPoint createDataPoint();

	/**
	 * Returns a new object of class '<em>Perceptron</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perceptron</em>'.
	 * @generated
	 */
	Perceptron createPerceptron();

	/**
	 * Returns a new object of class '<em>Neuron</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Neuron</em>'.
	 * @generated
	 */
	Neuron createNeuron();

	/**
	 * Returns a new object of class '<em>Input Layer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Input Layer</em>'.
	 * @generated
	 */
	InputLayer createInputLayer();

	/**
	 * Returns a new object of class '<em>Hidden Layer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Hidden Layer</em>'.
	 * @generated
	 */
	HiddenLayer createHiddenLayer();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	NeuralNetworkPackage getNeuralNetworkPackage();

} //NeuralNetworkFactory
