/**
 */
package neuralNetwork;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see neuralNetwork.NeuralNetworkFactory
 * @model kind="package"
 * @generated
 */
public interface NeuralNetworkPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "neuralNetwork";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.example.org/neuralNetwork";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "neuralNetwork";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	NeuralNetworkPackage eINSTANCE = neuralNetwork.impl.NeuralNetworkPackageImpl.init();

	/**
	 * The meta object id for the '{@link neuralNetwork.impl.NeuralNetworkImpl <em>Neural Network</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see neuralNetwork.impl.NeuralNetworkImpl
	 * @see neuralNetwork.impl.NeuralNetworkPackageImpl#getNeuralNetwork()
	 * @generated
	 */
	int NEURAL_NETWORK = 0;

	/**
	 * The feature id for the '<em><b>Activation Function</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEURAL_NETWORK__ACTIVATION_FUNCTION = 0;

	/**
	 * The feature id for the '<em><b>Inputlayer</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEURAL_NETWORK__INPUTLAYER = 1;

	/**
	 * The feature id for the '<em><b>Hiddenlayer</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEURAL_NETWORK__HIDDENLAYER = 2;

	/**
	 * The feature id for the '<em><b>Dataset</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEURAL_NETWORK__DATASET = 3;

	/**
	 * The number of structural features of the '<em>Neural Network</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEURAL_NETWORK_FEATURE_COUNT = 4;

	/**
	 * The operation id for the '<em>Test</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEURAL_NETWORK___TEST__DATASET = 0;

	/**
	 * The operation id for the '<em>Train</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEURAL_NETWORK___TRAIN__DATASET = 1;

	/**
	 * The number of operations of the '<em>Neural Network</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEURAL_NETWORK_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link neuralNetwork.impl.DataSetImpl <em>Data Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see neuralNetwork.impl.DataSetImpl
	 * @see neuralNetwork.impl.NeuralNetworkPackageImpl#getDataSet()
	 * @generated
	 */
	int DATA_SET = 1;

	/**
	 * The feature id for the '<em><b>Datapoint</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET__DATAPOINT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET__NAME = 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET__TYPE = 2;

	/**
	 * The number of structural features of the '<em>Data Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Data Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SET_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link neuralNetwork.impl.DataPointImpl <em>Data Point</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see neuralNetwork.impl.DataPointImpl
	 * @see neuralNetwork.impl.NeuralNetworkPackageImpl#getDataPoint()
	 * @generated
	 */
	int DATA_POINT = 2;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT__X = 0;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT__Y = 1;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT__LABEL = 2;

	/**
	 * The number of structural features of the '<em>Data Point</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Data Point</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link neuralNetwork.impl.PerceptronImpl <em>Perceptron</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see neuralNetwork.impl.PerceptronImpl
	 * @see neuralNetwork.impl.NeuralNetworkPackageImpl#getPerceptron()
	 * @generated
	 */
	int PERCEPTRON = 3;

	/**
	 * The feature id for the '<em><b>Feature Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERCEPTRON__FEATURE_TYPE = 0;

	/**
	 * The number of structural features of the '<em>Perceptron</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERCEPTRON_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Perceptron</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERCEPTRON_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link neuralNetwork.impl.NeuronImpl <em>Neuron</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see neuralNetwork.impl.NeuronImpl
	 * @see neuralNetwork.impl.NeuralNetworkPackageImpl#getNeuron()
	 * @generated
	 */
	int NEURON = 4;

	/**
	 * The number of structural features of the '<em>Neuron</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEURON_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Neuron</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEURON_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link neuralNetwork.impl.InputLayerImpl <em>Input Layer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see neuralNetwork.impl.InputLayerImpl
	 * @see neuralNetwork.impl.NeuralNetworkPackageImpl#getInputLayer()
	 * @generated
	 */
	int INPUT_LAYER = 5;

	/**
	 * The feature id for the '<em><b>Perceptrons</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_LAYER__PERCEPTRONS = 0;

	/**
	 * The number of structural features of the '<em>Input Layer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_LAYER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Input Layer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_LAYER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link neuralNetwork.impl.HiddenLayerImpl <em>Hidden Layer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see neuralNetwork.impl.HiddenLayerImpl
	 * @see neuralNetwork.impl.NeuralNetworkPackageImpl#getHiddenLayer()
	 * @generated
	 */
	int HIDDEN_LAYER = 6;

	/**
	 * The feature id for the '<em><b>Neurons</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HIDDEN_LAYER__NEURONS = 0;

	/**
	 * The number of structural features of the '<em>Hidden Layer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HIDDEN_LAYER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Hidden Layer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HIDDEN_LAYER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link neuralNetwork.FeatureType <em>Feature Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see neuralNetwork.FeatureType
	 * @see neuralNetwork.impl.NeuralNetworkPackageImpl#getFeatureType()
	 * @generated
	 */
	int FEATURE_TYPE = 7;

	/**
	 * The meta object id for the '{@link neuralNetwork.ActivationFunction <em>Activation Function</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see neuralNetwork.ActivationFunction
	 * @see neuralNetwork.impl.NeuralNetworkPackageImpl#getActivationFunction()
	 * @generated
	 */
	int ACTIVATION_FUNCTION = 8;


	/**
	 * The meta object id for the '{@link neuralNetwork.DatasetType <em>Dataset Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see neuralNetwork.DatasetType
	 * @see neuralNetwork.impl.NeuralNetworkPackageImpl#getDatasetType()
	 * @generated
	 */
	int DATASET_TYPE = 9;


	/**
	 * Returns the meta object for class '{@link neuralNetwork.NeuralNetwork <em>Neural Network</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Neural Network</em>'.
	 * @see neuralNetwork.NeuralNetwork
	 * @generated
	 */
	EClass getNeuralNetwork();

	/**
	 * Returns the meta object for the attribute '{@link neuralNetwork.NeuralNetwork#getActivationFunction <em>Activation Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Activation Function</em>'.
	 * @see neuralNetwork.NeuralNetwork#getActivationFunction()
	 * @see #getNeuralNetwork()
	 * @generated
	 */
	EAttribute getNeuralNetwork_ActivationFunction();

	/**
	 * Returns the meta object for the containment reference '{@link neuralNetwork.NeuralNetwork#getInputlayer <em>Inputlayer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Inputlayer</em>'.
	 * @see neuralNetwork.NeuralNetwork#getInputlayer()
	 * @see #getNeuralNetwork()
	 * @generated
	 */
	EReference getNeuralNetwork_Inputlayer();

	/**
	 * Returns the meta object for the containment reference list '{@link neuralNetwork.NeuralNetwork#getHiddenlayer <em>Hiddenlayer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Hiddenlayer</em>'.
	 * @see neuralNetwork.NeuralNetwork#getHiddenlayer()
	 * @see #getNeuralNetwork()
	 * @generated
	 */
	EReference getNeuralNetwork_Hiddenlayer();

	/**
	 * Returns the meta object for the containment reference '{@link neuralNetwork.NeuralNetwork#getDataset <em>Dataset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Dataset</em>'.
	 * @see neuralNetwork.NeuralNetwork#getDataset()
	 * @see #getNeuralNetwork()
	 * @generated
	 */
	EReference getNeuralNetwork_Dataset();

	/**
	 * Returns the meta object for the '{@link neuralNetwork.NeuralNetwork#test(neuralNetwork.DataSet) <em>Test</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Test</em>' operation.
	 * @see neuralNetwork.NeuralNetwork#test(neuralNetwork.DataSet)
	 * @generated
	 */
	EOperation getNeuralNetwork__Test__DataSet();

	/**
	 * Returns the meta object for the '{@link neuralNetwork.NeuralNetwork#train(neuralNetwork.DataSet) <em>Train</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Train</em>' operation.
	 * @see neuralNetwork.NeuralNetwork#train(neuralNetwork.DataSet)
	 * @generated
	 */
	EOperation getNeuralNetwork__Train__DataSet();

	/**
	 * Returns the meta object for class '{@link neuralNetwork.DataSet <em>Data Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Set</em>'.
	 * @see neuralNetwork.DataSet
	 * @generated
	 */
	EClass getDataSet();

	/**
	 * Returns the meta object for the containment reference list '{@link neuralNetwork.DataSet#getDatapoint <em>Datapoint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Datapoint</em>'.
	 * @see neuralNetwork.DataSet#getDatapoint()
	 * @see #getDataSet()
	 * @generated
	 */
	EReference getDataSet_Datapoint();

	/**
	 * Returns the meta object for the attribute '{@link neuralNetwork.DataSet#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see neuralNetwork.DataSet#getName()
	 * @see #getDataSet()
	 * @generated
	 */
	EAttribute getDataSet_Name();

	/**
	 * Returns the meta object for the attribute '{@link neuralNetwork.DataSet#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see neuralNetwork.DataSet#getType()
	 * @see #getDataSet()
	 * @generated
	 */
	EAttribute getDataSet_Type();

	/**
	 * Returns the meta object for class '{@link neuralNetwork.DataPoint <em>Data Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Point</em>'.
	 * @see neuralNetwork.DataPoint
	 * @generated
	 */
	EClass getDataPoint();

	/**
	 * Returns the meta object for the attribute '{@link neuralNetwork.DataPoint#getX <em>X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>X</em>'.
	 * @see neuralNetwork.DataPoint#getX()
	 * @see #getDataPoint()
	 * @generated
	 */
	EAttribute getDataPoint_X();

	/**
	 * Returns the meta object for the attribute '{@link neuralNetwork.DataPoint#getY <em>Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Y</em>'.
	 * @see neuralNetwork.DataPoint#getY()
	 * @see #getDataPoint()
	 * @generated
	 */
	EAttribute getDataPoint_Y();

	/**
	 * Returns the meta object for the attribute '{@link neuralNetwork.DataPoint#isLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see neuralNetwork.DataPoint#isLabel()
	 * @see #getDataPoint()
	 * @generated
	 */
	EAttribute getDataPoint_Label();

	/**
	 * Returns the meta object for class '{@link neuralNetwork.Perceptron <em>Perceptron</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perceptron</em>'.
	 * @see neuralNetwork.Perceptron
	 * @generated
	 */
	EClass getPerceptron();

	/**
	 * Returns the meta object for the attribute '{@link neuralNetwork.Perceptron#getFeatureType <em>Feature Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Feature Type</em>'.
	 * @see neuralNetwork.Perceptron#getFeatureType()
	 * @see #getPerceptron()
	 * @generated
	 */
	EAttribute getPerceptron_FeatureType();

	/**
	 * Returns the meta object for class '{@link neuralNetwork.Neuron <em>Neuron</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Neuron</em>'.
	 * @see neuralNetwork.Neuron
	 * @generated
	 */
	EClass getNeuron();

	/**
	 * Returns the meta object for class '{@link neuralNetwork.InputLayer <em>Input Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Input Layer</em>'.
	 * @see neuralNetwork.InputLayer
	 * @generated
	 */
	EClass getInputLayer();

	/**
	 * Returns the meta object for the containment reference list '{@link neuralNetwork.InputLayer#getPerceptrons <em>Perceptrons</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Perceptrons</em>'.
	 * @see neuralNetwork.InputLayer#getPerceptrons()
	 * @see #getInputLayer()
	 * @generated
	 */
	EReference getInputLayer_Perceptrons();

	/**
	 * Returns the meta object for class '{@link neuralNetwork.HiddenLayer <em>Hidden Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hidden Layer</em>'.
	 * @see neuralNetwork.HiddenLayer
	 * @generated
	 */
	EClass getHiddenLayer();

	/**
	 * Returns the meta object for the containment reference list '{@link neuralNetwork.HiddenLayer#getNeurons <em>Neurons</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Neurons</em>'.
	 * @see neuralNetwork.HiddenLayer#getNeurons()
	 * @see #getHiddenLayer()
	 * @generated
	 */
	EReference getHiddenLayer_Neurons();

	/**
	 * Returns the meta object for enum '{@link neuralNetwork.FeatureType <em>Feature Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Feature Type</em>'.
	 * @see neuralNetwork.FeatureType
	 * @generated
	 */
	EEnum getFeatureType();

	/**
	 * Returns the meta object for enum '{@link neuralNetwork.ActivationFunction <em>Activation Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Activation Function</em>'.
	 * @see neuralNetwork.ActivationFunction
	 * @generated
	 */
	EEnum getActivationFunction();

	/**
	 * Returns the meta object for enum '{@link neuralNetwork.DatasetType <em>Dataset Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Dataset Type</em>'.
	 * @see neuralNetwork.DatasetType
	 * @generated
	 */
	EEnum getDatasetType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	NeuralNetworkFactory getNeuralNetworkFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link neuralNetwork.impl.NeuralNetworkImpl <em>Neural Network</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see neuralNetwork.impl.NeuralNetworkImpl
		 * @see neuralNetwork.impl.NeuralNetworkPackageImpl#getNeuralNetwork()
		 * @generated
		 */
		EClass NEURAL_NETWORK = eINSTANCE.getNeuralNetwork();

		/**
		 * The meta object literal for the '<em><b>Activation Function</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NEURAL_NETWORK__ACTIVATION_FUNCTION = eINSTANCE.getNeuralNetwork_ActivationFunction();

		/**
		 * The meta object literal for the '<em><b>Inputlayer</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NEURAL_NETWORK__INPUTLAYER = eINSTANCE.getNeuralNetwork_Inputlayer();

		/**
		 * The meta object literal for the '<em><b>Hiddenlayer</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NEURAL_NETWORK__HIDDENLAYER = eINSTANCE.getNeuralNetwork_Hiddenlayer();

		/**
		 * The meta object literal for the '<em><b>Dataset</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NEURAL_NETWORK__DATASET = eINSTANCE.getNeuralNetwork_Dataset();

		/**
		 * The meta object literal for the '<em><b>Test</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation NEURAL_NETWORK___TEST__DATASET = eINSTANCE.getNeuralNetwork__Test__DataSet();

		/**
		 * The meta object literal for the '<em><b>Train</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation NEURAL_NETWORK___TRAIN__DATASET = eINSTANCE.getNeuralNetwork__Train__DataSet();

		/**
		 * The meta object literal for the '{@link neuralNetwork.impl.DataSetImpl <em>Data Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see neuralNetwork.impl.DataSetImpl
		 * @see neuralNetwork.impl.NeuralNetworkPackageImpl#getDataSet()
		 * @generated
		 */
		EClass DATA_SET = eINSTANCE.getDataSet();

		/**
		 * The meta object literal for the '<em><b>Datapoint</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_SET__DATAPOINT = eINSTANCE.getDataSet_Datapoint();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_SET__NAME = eINSTANCE.getDataSet_Name();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_SET__TYPE = eINSTANCE.getDataSet_Type();

		/**
		 * The meta object literal for the '{@link neuralNetwork.impl.DataPointImpl <em>Data Point</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see neuralNetwork.impl.DataPointImpl
		 * @see neuralNetwork.impl.NeuralNetworkPackageImpl#getDataPoint()
		 * @generated
		 */
		EClass DATA_POINT = eINSTANCE.getDataPoint();

		/**
		 * The meta object literal for the '<em><b>X</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_POINT__X = eINSTANCE.getDataPoint_X();

		/**
		 * The meta object literal for the '<em><b>Y</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_POINT__Y = eINSTANCE.getDataPoint_Y();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_POINT__LABEL = eINSTANCE.getDataPoint_Label();

		/**
		 * The meta object literal for the '{@link neuralNetwork.impl.PerceptronImpl <em>Perceptron</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see neuralNetwork.impl.PerceptronImpl
		 * @see neuralNetwork.impl.NeuralNetworkPackageImpl#getPerceptron()
		 * @generated
		 */
		EClass PERCEPTRON = eINSTANCE.getPerceptron();

		/**
		 * The meta object literal for the '<em><b>Feature Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERCEPTRON__FEATURE_TYPE = eINSTANCE.getPerceptron_FeatureType();

		/**
		 * The meta object literal for the '{@link neuralNetwork.impl.NeuronImpl <em>Neuron</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see neuralNetwork.impl.NeuronImpl
		 * @see neuralNetwork.impl.NeuralNetworkPackageImpl#getNeuron()
		 * @generated
		 */
		EClass NEURON = eINSTANCE.getNeuron();

		/**
		 * The meta object literal for the '{@link neuralNetwork.impl.InputLayerImpl <em>Input Layer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see neuralNetwork.impl.InputLayerImpl
		 * @see neuralNetwork.impl.NeuralNetworkPackageImpl#getInputLayer()
		 * @generated
		 */
		EClass INPUT_LAYER = eINSTANCE.getInputLayer();

		/**
		 * The meta object literal for the '<em><b>Perceptrons</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INPUT_LAYER__PERCEPTRONS = eINSTANCE.getInputLayer_Perceptrons();

		/**
		 * The meta object literal for the '{@link neuralNetwork.impl.HiddenLayerImpl <em>Hidden Layer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see neuralNetwork.impl.HiddenLayerImpl
		 * @see neuralNetwork.impl.NeuralNetworkPackageImpl#getHiddenLayer()
		 * @generated
		 */
		EClass HIDDEN_LAYER = eINSTANCE.getHiddenLayer();

		/**
		 * The meta object literal for the '<em><b>Neurons</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HIDDEN_LAYER__NEURONS = eINSTANCE.getHiddenLayer_Neurons();

		/**
		 * The meta object literal for the '{@link neuralNetwork.FeatureType <em>Feature Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see neuralNetwork.FeatureType
		 * @see neuralNetwork.impl.NeuralNetworkPackageImpl#getFeatureType()
		 * @generated
		 */
		EEnum FEATURE_TYPE = eINSTANCE.getFeatureType();

		/**
		 * The meta object literal for the '{@link neuralNetwork.ActivationFunction <em>Activation Function</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see neuralNetwork.ActivationFunction
		 * @see neuralNetwork.impl.NeuralNetworkPackageImpl#getActivationFunction()
		 * @generated
		 */
		EEnum ACTIVATION_FUNCTION = eINSTANCE.getActivationFunction();

		/**
		 * The meta object literal for the '{@link neuralNetwork.DatasetType <em>Dataset Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see neuralNetwork.DatasetType
		 * @see neuralNetwork.impl.NeuralNetworkPackageImpl#getDatasetType()
		 * @generated
		 */
		EEnum DATASET_TYPE = eINSTANCE.getDatasetType();

	}

} //NeuralNetworkPackage
