/**
 */
package neuralNetwork;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Neuron</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see neuralNetwork.NeuralNetworkPackage#getNeuron()
 * @model
 * @generated
 */
public interface Neuron extends EObject {

} // Neuron
