/**
 */
package neuralNetwork;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Perceptron</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link neuralNetwork.Perceptron#getFeatureType <em>Feature Type</em>}</li>
 * </ul>
 *
 * @see neuralNetwork.NeuralNetworkPackage#getPerceptron()
 * @model
 * @generated
 */
public interface Perceptron extends EObject {
	/**
	 * Returns the value of the '<em><b>Feature Type</b></em>' attribute.
	 * The literals are from the enumeration {@link neuralNetwork.FeatureType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature Type</em>' attribute.
	 * @see neuralNetwork.FeatureType
	 * @see #setFeatureType(FeatureType)
	 * @see neuralNetwork.NeuralNetworkPackage#getPerceptron_FeatureType()
	 * @model
	 * @generated
	 */
	FeatureType getFeatureType();

	/**
	 * Sets the value of the '{@link neuralNetwork.Perceptron#getFeatureType <em>Feature Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature Type</em>' attribute.
	 * @see neuralNetwork.FeatureType
	 * @see #getFeatureType()
	 * @generated
	 */
	void setFeatureType(FeatureType value);

} // Perceptron
