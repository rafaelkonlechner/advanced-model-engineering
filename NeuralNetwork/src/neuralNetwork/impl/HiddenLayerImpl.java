/**
 */
package neuralNetwork.impl;

import java.util.Collection;

import neuralNetwork.HiddenLayer;
import neuralNetwork.NeuralNetworkPackage;
import neuralNetwork.Neuron;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hidden Layer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link neuralNetwork.impl.HiddenLayerImpl#getNeurons <em>Neurons</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HiddenLayerImpl extends MinimalEObjectImpl.Container implements HiddenLayer {
	/**
	 * The cached value of the '{@link #getNeurons() <em>Neurons</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNeurons()
	 * @generated
	 * @ordered
	 */
	protected EList<Neuron> neurons;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HiddenLayerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return NeuralNetworkPackage.Literals.HIDDEN_LAYER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Neuron> getNeurons() {
		if (neurons == null) {
			neurons = new EObjectContainmentEList<Neuron>(Neuron.class, this, NeuralNetworkPackage.HIDDEN_LAYER__NEURONS);
		}
		return neurons;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case NeuralNetworkPackage.HIDDEN_LAYER__NEURONS:
				return ((InternalEList<?>)getNeurons()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case NeuralNetworkPackage.HIDDEN_LAYER__NEURONS:
				return getNeurons();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case NeuralNetworkPackage.HIDDEN_LAYER__NEURONS:
				getNeurons().clear();
				getNeurons().addAll((Collection<? extends Neuron>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case NeuralNetworkPackage.HIDDEN_LAYER__NEURONS:
				getNeurons().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case NeuralNetworkPackage.HIDDEN_LAYER__NEURONS:
				return neurons != null && !neurons.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //HiddenLayerImpl
