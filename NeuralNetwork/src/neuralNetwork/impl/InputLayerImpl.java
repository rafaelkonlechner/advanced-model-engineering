/**
 */
package neuralNetwork.impl;

import java.util.Collection;

import neuralNetwork.InputLayer;
import neuralNetwork.NeuralNetworkPackage;
import neuralNetwork.Perceptron;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Input Layer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link neuralNetwork.impl.InputLayerImpl#getPerceptrons <em>Perceptrons</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InputLayerImpl extends MinimalEObjectImpl.Container implements InputLayer {
	/**
	 * The cached value of the '{@link #getPerceptrons() <em>Perceptrons</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPerceptrons()
	 * @generated
	 * @ordered
	 */
	protected EList<Perceptron> perceptrons;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InputLayerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return NeuralNetworkPackage.Literals.INPUT_LAYER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Perceptron> getPerceptrons() {
		if (perceptrons == null) {
			perceptrons = new EObjectContainmentEList<Perceptron>(Perceptron.class, this, NeuralNetworkPackage.INPUT_LAYER__PERCEPTRONS);
		}
		return perceptrons;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case NeuralNetworkPackage.INPUT_LAYER__PERCEPTRONS:
				return ((InternalEList<?>)getPerceptrons()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case NeuralNetworkPackage.INPUT_LAYER__PERCEPTRONS:
				return getPerceptrons();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case NeuralNetworkPackage.INPUT_LAYER__PERCEPTRONS:
				getPerceptrons().clear();
				getPerceptrons().addAll((Collection<? extends Perceptron>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case NeuralNetworkPackage.INPUT_LAYER__PERCEPTRONS:
				getPerceptrons().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case NeuralNetworkPackage.INPUT_LAYER__PERCEPTRONS:
				return perceptrons != null && !perceptrons.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //InputLayerImpl
