/**
 */
package neuralNetwork.impl;

import neuralNetwork.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class NeuralNetworkFactoryImpl extends EFactoryImpl implements NeuralNetworkFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static NeuralNetworkFactory init() {
		try {
			NeuralNetworkFactory theNeuralNetworkFactory = (NeuralNetworkFactory)EPackage.Registry.INSTANCE.getEFactory(NeuralNetworkPackage.eNS_URI);
			if (theNeuralNetworkFactory != null) {
				return theNeuralNetworkFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new NeuralNetworkFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NeuralNetworkFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case NeuralNetworkPackage.NEURAL_NETWORK: return createNeuralNetwork();
			case NeuralNetworkPackage.DATA_SET: return createDataSet();
			case NeuralNetworkPackage.DATA_POINT: return createDataPoint();
			case NeuralNetworkPackage.PERCEPTRON: return createPerceptron();
			case NeuralNetworkPackage.NEURON: return createNeuron();
			case NeuralNetworkPackage.INPUT_LAYER: return createInputLayer();
			case NeuralNetworkPackage.HIDDEN_LAYER: return createHiddenLayer();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case NeuralNetworkPackage.FEATURE_TYPE:
				return createFeatureTypeFromString(eDataType, initialValue);
			case NeuralNetworkPackage.ACTIVATION_FUNCTION:
				return createActivationFunctionFromString(eDataType, initialValue);
			case NeuralNetworkPackage.DATASET_TYPE:
				return createDatasetTypeFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case NeuralNetworkPackage.FEATURE_TYPE:
				return convertFeatureTypeToString(eDataType, instanceValue);
			case NeuralNetworkPackage.ACTIVATION_FUNCTION:
				return convertActivationFunctionToString(eDataType, instanceValue);
			case NeuralNetworkPackage.DATASET_TYPE:
				return convertDatasetTypeToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NeuralNetwork createNeuralNetwork() {
		NeuralNetworkImpl neuralNetwork = new NeuralNetworkImpl();
		return neuralNetwork;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataSet createDataSet() {
		DataSetImpl dataSet = new DataSetImpl();
		return dataSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataPoint createDataPoint() {
		DataPointImpl dataPoint = new DataPointImpl();
		return dataPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Perceptron createPerceptron() {
		PerceptronImpl perceptron = new PerceptronImpl();
		return perceptron;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Neuron createNeuron() {
		NeuronImpl neuron = new NeuronImpl();
		return neuron;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InputLayer createInputLayer() {
		InputLayerImpl inputLayer = new InputLayerImpl();
		return inputLayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HiddenLayer createHiddenLayer() {
		HiddenLayerImpl hiddenLayer = new HiddenLayerImpl();
		return hiddenLayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureType createFeatureTypeFromString(EDataType eDataType, String initialValue) {
		FeatureType result = FeatureType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFeatureTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActivationFunction createActivationFunctionFromString(EDataType eDataType, String initialValue) {
		ActivationFunction result = ActivationFunction.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertActivationFunctionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DatasetType createDatasetTypeFromString(EDataType eDataType, String initialValue) {
		DatasetType result = DatasetType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDatasetTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NeuralNetworkPackage getNeuralNetworkPackage() {
		return (NeuralNetworkPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static NeuralNetworkPackage getPackage() {
		return NeuralNetworkPackage.eINSTANCE;
	}

} //NeuralNetworkFactoryImpl
