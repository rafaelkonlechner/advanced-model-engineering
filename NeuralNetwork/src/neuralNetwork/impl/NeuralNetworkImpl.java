/**
 */
package neuralNetwork.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import neuralNetwork.ActivationFunction;
import neuralNetwork.DataSet;
import neuralNetwork.HiddenLayer;
import neuralNetwork.InputLayer;
import neuralNetwork.NeuralNetwork;
import neuralNetwork.NeuralNetworkPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Neural Network</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link neuralNetwork.impl.NeuralNetworkImpl#getActivationFunction <em>Activation Function</em>}</li>
 *   <li>{@link neuralNetwork.impl.NeuralNetworkImpl#getInputlayer <em>Inputlayer</em>}</li>
 *   <li>{@link neuralNetwork.impl.NeuralNetworkImpl#getHiddenlayer <em>Hiddenlayer</em>}</li>
 *   <li>{@link neuralNetwork.impl.NeuralNetworkImpl#getDataset <em>Dataset</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NeuralNetworkImpl extends MinimalEObjectImpl.Container implements NeuralNetwork {
	/**
	 * The default value of the '{@link #getActivationFunction() <em>Activation Function</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActivationFunction()
	 * @generated
	 * @ordered
	 */
	protected static final ActivationFunction ACTIVATION_FUNCTION_EDEFAULT = ActivationFunction.TANH;

	/**
	 * The cached value of the '{@link #getActivationFunction() <em>Activation Function</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActivationFunction()
	 * @generated
	 * @ordered
	 */
	protected ActivationFunction activationFunction = ACTIVATION_FUNCTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInputlayer() <em>Inputlayer</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputlayer()
	 * @generated
	 * @ordered
	 */
	protected InputLayer inputlayer;

	/**
	 * The cached value of the '{@link #getHiddenlayer() <em>Hiddenlayer</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHiddenlayer()
	 * @generated
	 * @ordered
	 */
	protected EList<HiddenLayer> hiddenlayer;

	/**
	 * The cached value of the '{@link #getDataset() <em>Dataset</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataset()
	 * @generated
	 * @ordered
	 */
	protected DataSet dataset;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NeuralNetworkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return NeuralNetworkPackage.Literals.NEURAL_NETWORK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActivationFunction getActivationFunction() {
		return activationFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActivationFunction(ActivationFunction newActivationFunction) {
		ActivationFunction oldActivationFunction = activationFunction;
		activationFunction = newActivationFunction == null ? ACTIVATION_FUNCTION_EDEFAULT : newActivationFunction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, NeuralNetworkPackage.NEURAL_NETWORK__ACTIVATION_FUNCTION, oldActivationFunction, activationFunction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InputLayer getInputlayer() {
		return inputlayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInputlayer(InputLayer newInputlayer, NotificationChain msgs) {
		InputLayer oldInputlayer = inputlayer;
		inputlayer = newInputlayer;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, NeuralNetworkPackage.NEURAL_NETWORK__INPUTLAYER, oldInputlayer, newInputlayer);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInputlayer(InputLayer newInputlayer) {
		if (newInputlayer != inputlayer) {
			NotificationChain msgs = null;
			if (inputlayer != null)
				msgs = ((InternalEObject)inputlayer).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - NeuralNetworkPackage.NEURAL_NETWORK__INPUTLAYER, null, msgs);
			if (newInputlayer != null)
				msgs = ((InternalEObject)newInputlayer).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - NeuralNetworkPackage.NEURAL_NETWORK__INPUTLAYER, null, msgs);
			msgs = basicSetInputlayer(newInputlayer, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, NeuralNetworkPackage.NEURAL_NETWORK__INPUTLAYER, newInputlayer, newInputlayer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<HiddenLayer> getHiddenlayer() {
		if (hiddenlayer == null) {
			hiddenlayer = new EObjectContainmentEList<HiddenLayer>(HiddenLayer.class, this, NeuralNetworkPackage.NEURAL_NETWORK__HIDDENLAYER);
		}
		return hiddenlayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataSet getDataset() {
		return dataset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataset(DataSet newDataset, NotificationChain msgs) {
		DataSet oldDataset = dataset;
		dataset = newDataset;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, NeuralNetworkPackage.NEURAL_NETWORK__DATASET, oldDataset, newDataset);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataset(DataSet newDataset) {
		if (newDataset != dataset) {
			NotificationChain msgs = null;
			if (dataset != null)
				msgs = ((InternalEObject)dataset).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - NeuralNetworkPackage.NEURAL_NETWORK__DATASET, null, msgs);
			if (newDataset != null)
				msgs = ((InternalEObject)newDataset).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - NeuralNetworkPackage.NEURAL_NETWORK__DATASET, null, msgs);
			msgs = basicSetDataset(newDataset, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, NeuralNetworkPackage.NEURAL_NETWORK__DATASET, newDataset, newDataset));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void test(DataSet dataset) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void train(DataSet dataset) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case NeuralNetworkPackage.NEURAL_NETWORK__INPUTLAYER:
				return basicSetInputlayer(null, msgs);
			case NeuralNetworkPackage.NEURAL_NETWORK__HIDDENLAYER:
				return ((InternalEList<?>)getHiddenlayer()).basicRemove(otherEnd, msgs);
			case NeuralNetworkPackage.NEURAL_NETWORK__DATASET:
				return basicSetDataset(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case NeuralNetworkPackage.NEURAL_NETWORK__ACTIVATION_FUNCTION:
				return getActivationFunction();
			case NeuralNetworkPackage.NEURAL_NETWORK__INPUTLAYER:
				return getInputlayer();
			case NeuralNetworkPackage.NEURAL_NETWORK__HIDDENLAYER:
				return getHiddenlayer();
			case NeuralNetworkPackage.NEURAL_NETWORK__DATASET:
				return getDataset();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case NeuralNetworkPackage.NEURAL_NETWORK__ACTIVATION_FUNCTION:
				setActivationFunction((ActivationFunction)newValue);
				return;
			case NeuralNetworkPackage.NEURAL_NETWORK__INPUTLAYER:
				setInputlayer((InputLayer)newValue);
				return;
			case NeuralNetworkPackage.NEURAL_NETWORK__HIDDENLAYER:
				getHiddenlayer().clear();
				getHiddenlayer().addAll((Collection<? extends HiddenLayer>)newValue);
				return;
			case NeuralNetworkPackage.NEURAL_NETWORK__DATASET:
				setDataset((DataSet)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case NeuralNetworkPackage.NEURAL_NETWORK__ACTIVATION_FUNCTION:
				setActivationFunction(ACTIVATION_FUNCTION_EDEFAULT);
				return;
			case NeuralNetworkPackage.NEURAL_NETWORK__INPUTLAYER:
				setInputlayer((InputLayer)null);
				return;
			case NeuralNetworkPackage.NEURAL_NETWORK__HIDDENLAYER:
				getHiddenlayer().clear();
				return;
			case NeuralNetworkPackage.NEURAL_NETWORK__DATASET:
				setDataset((DataSet)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case NeuralNetworkPackage.NEURAL_NETWORK__ACTIVATION_FUNCTION:
				return activationFunction != ACTIVATION_FUNCTION_EDEFAULT;
			case NeuralNetworkPackage.NEURAL_NETWORK__INPUTLAYER:
				return inputlayer != null;
			case NeuralNetworkPackage.NEURAL_NETWORK__HIDDENLAYER:
				return hiddenlayer != null && !hiddenlayer.isEmpty();
			case NeuralNetworkPackage.NEURAL_NETWORK__DATASET:
				return dataset != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case NeuralNetworkPackage.NEURAL_NETWORK___TEST__DATASET:
				test((DataSet)arguments.get(0));
				return null;
			case NeuralNetworkPackage.NEURAL_NETWORK___TRAIN__DATASET:
				train((DataSet)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (activationFunction: ");
		result.append(activationFunction);
		result.append(')');
		return result.toString();
	}

} //NeuralNetworkImpl
