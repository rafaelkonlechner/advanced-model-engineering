/**
 */
package neuralNetwork.impl;

import neuralNetwork.ActivationFunction;
import neuralNetwork.DataPoint;
import neuralNetwork.DataSet;
import neuralNetwork.DatasetType;
import neuralNetwork.FeatureType;
import neuralNetwork.HiddenLayer;
import neuralNetwork.InputLayer;
import neuralNetwork.NeuralNetwork;
import neuralNetwork.NeuralNetworkFactory;
import neuralNetwork.NeuralNetworkPackage;
import neuralNetwork.Neuron;
import neuralNetwork.Perceptron;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class NeuralNetworkPackageImpl extends EPackageImpl implements NeuralNetworkPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass neuralNetworkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataSetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataPointEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass perceptronEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass neuronEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass inputLayerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hiddenLayerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum featureTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum activationFunctionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum datasetTypeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see neuralNetwork.NeuralNetworkPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private NeuralNetworkPackageImpl() {
		super(eNS_URI, NeuralNetworkFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link NeuralNetworkPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static NeuralNetworkPackage init() {
		if (isInited) return (NeuralNetworkPackage)EPackage.Registry.INSTANCE.getEPackage(NeuralNetworkPackage.eNS_URI);

		// Obtain or create and register package
		NeuralNetworkPackageImpl theNeuralNetworkPackage = (NeuralNetworkPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof NeuralNetworkPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new NeuralNetworkPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theNeuralNetworkPackage.createPackageContents();

		// Initialize created meta-data
		theNeuralNetworkPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theNeuralNetworkPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(NeuralNetworkPackage.eNS_URI, theNeuralNetworkPackage);
		return theNeuralNetworkPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNeuralNetwork() {
		return neuralNetworkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNeuralNetwork_ActivationFunction() {
		return (EAttribute)neuralNetworkEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNeuralNetwork_Inputlayer() {
		return (EReference)neuralNetworkEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNeuralNetwork_Hiddenlayer() {
		return (EReference)neuralNetworkEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNeuralNetwork_Dataset() {
		return (EReference)neuralNetworkEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getNeuralNetwork__Test__DataSet() {
		return neuralNetworkEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getNeuralNetwork__Train__DataSet() {
		return neuralNetworkEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataSet() {
		return dataSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataSet_Datapoint() {
		return (EReference)dataSetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataSet_Name() {
		return (EAttribute)dataSetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataSet_Type() {
		return (EAttribute)dataSetEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataPoint() {
		return dataPointEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataPoint_X() {
		return (EAttribute)dataPointEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataPoint_Y() {
		return (EAttribute)dataPointEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDataPoint_Label() {
		return (EAttribute)dataPointEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerceptron() {
		return perceptronEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPerceptron_FeatureType() {
		return (EAttribute)perceptronEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNeuron() {
		return neuronEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInputLayer() {
		return inputLayerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInputLayer_Perceptrons() {
		return (EReference)inputLayerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHiddenLayer() {
		return hiddenLayerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHiddenLayer_Neurons() {
		return (EReference)hiddenLayerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getFeatureType() {
		return featureTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getActivationFunction() {
		return activationFunctionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDatasetType() {
		return datasetTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NeuralNetworkFactory getNeuralNetworkFactory() {
		return (NeuralNetworkFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		neuralNetworkEClass = createEClass(NEURAL_NETWORK);
		createEAttribute(neuralNetworkEClass, NEURAL_NETWORK__ACTIVATION_FUNCTION);
		createEReference(neuralNetworkEClass, NEURAL_NETWORK__INPUTLAYER);
		createEReference(neuralNetworkEClass, NEURAL_NETWORK__HIDDENLAYER);
		createEReference(neuralNetworkEClass, NEURAL_NETWORK__DATASET);
		createEOperation(neuralNetworkEClass, NEURAL_NETWORK___TEST__DATASET);
		createEOperation(neuralNetworkEClass, NEURAL_NETWORK___TRAIN__DATASET);

		dataSetEClass = createEClass(DATA_SET);
		createEReference(dataSetEClass, DATA_SET__DATAPOINT);
		createEAttribute(dataSetEClass, DATA_SET__NAME);
		createEAttribute(dataSetEClass, DATA_SET__TYPE);

		dataPointEClass = createEClass(DATA_POINT);
		createEAttribute(dataPointEClass, DATA_POINT__X);
		createEAttribute(dataPointEClass, DATA_POINT__Y);
		createEAttribute(dataPointEClass, DATA_POINT__LABEL);

		perceptronEClass = createEClass(PERCEPTRON);
		createEAttribute(perceptronEClass, PERCEPTRON__FEATURE_TYPE);

		neuronEClass = createEClass(NEURON);

		inputLayerEClass = createEClass(INPUT_LAYER);
		createEReference(inputLayerEClass, INPUT_LAYER__PERCEPTRONS);

		hiddenLayerEClass = createEClass(HIDDEN_LAYER);
		createEReference(hiddenLayerEClass, HIDDEN_LAYER__NEURONS);

		// Create enums
		featureTypeEEnum = createEEnum(FEATURE_TYPE);
		activationFunctionEEnum = createEEnum(ACTIVATION_FUNCTION);
		datasetTypeEEnum = createEEnum(DATASET_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(neuralNetworkEClass, NeuralNetwork.class, "NeuralNetwork", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNeuralNetwork_ActivationFunction(), this.getActivationFunction(), "activationFunction", null, 0, 1, NeuralNetwork.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNeuralNetwork_Inputlayer(), this.getInputLayer(), null, "inputlayer", null, 1, 1, NeuralNetwork.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNeuralNetwork_Hiddenlayer(), this.getHiddenLayer(), null, "hiddenlayer", null, 0, 4, NeuralNetwork.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNeuralNetwork_Dataset(), this.getDataSet(), null, "dataset", null, 1, 1, NeuralNetwork.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getNeuralNetwork__Test__DataSet(), null, "test", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDataSet(), "dataset", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getNeuralNetwork__Train__DataSet(), null, "train", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDataSet(), "dataset", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(dataSetEClass, DataSet.class, "DataSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataSet_Datapoint(), this.getDataPoint(), null, "datapoint", null, 0, -1, DataSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDataSet_Name(), ecorePackage.getEString(), "name", null, 0, 1, DataSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDataSet_Type(), this.getDatasetType(), "type", null, 0, 1, DataSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dataPointEClass, DataPoint.class, "DataPoint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDataPoint_X(), ecorePackage.getEDouble(), "X", null, 0, 1, DataPoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDataPoint_Y(), ecorePackage.getEDouble(), "Y", null, 0, 1, DataPoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDataPoint_Label(), ecorePackage.getEBoolean(), "Label", null, 0, 1, DataPoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(perceptronEClass, Perceptron.class, "Perceptron", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPerceptron_FeatureType(), this.getFeatureType(), "featureType", null, 0, 1, Perceptron.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(neuronEClass, Neuron.class, "Neuron", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(inputLayerEClass, InputLayer.class, "InputLayer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInputLayer_Perceptrons(), this.getPerceptron(), null, "perceptrons", null, 0, 4, InputLayer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hiddenLayerEClass, HiddenLayer.class, "HiddenLayer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHiddenLayer_Neurons(), this.getNeuron(), null, "neurons", null, 1, 8, HiddenLayer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(featureTypeEEnum, FeatureType.class, "FeatureType");
		addEEnumLiteral(featureTypeEEnum, FeatureType.X);
		addEEnumLiteral(featureTypeEEnum, FeatureType.Y);
		addEEnumLiteral(featureTypeEEnum, FeatureType.XX);
		addEEnumLiteral(featureTypeEEnum, FeatureType.YY);
		addEEnumLiteral(featureTypeEEnum, FeatureType.XY);

		initEEnum(activationFunctionEEnum, ActivationFunction.class, "ActivationFunction");
		addEEnumLiteral(activationFunctionEEnum, ActivationFunction.TANH);
		addEEnumLiteral(activationFunctionEEnum, ActivationFunction.SIGMOID);
		addEEnumLiteral(activationFunctionEEnum, ActivationFunction.LINEAR);

		initEEnum(datasetTypeEEnum, DatasetType.class, "DatasetType");
		addEEnumLiteral(datasetTypeEEnum, DatasetType.CIRCLE);
		addEEnumLiteral(datasetTypeEEnum, DatasetType.XOR);
		addEEnumLiteral(datasetTypeEEnum, DatasetType.GAUSS);
		addEEnumLiteral(datasetTypeEEnum, DatasetType.SPIRAL);

		// Create resource
		createResource(eNS_URI);
	}

} //NeuralNetworkPackageImpl
