/**
 */
package neuralNetwork.impl;

import neuralNetwork.NeuralNetworkPackage;
import neuralNetwork.Neuron;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Neuron</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class NeuronImpl extends MinimalEObjectImpl.Container implements Neuron {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NeuronImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return NeuralNetworkPackage.Literals.NEURON;
	}

} //NeuronImpl
