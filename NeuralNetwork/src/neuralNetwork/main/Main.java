package neuralNetwork.main;

import java.io.File;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import neuralNetwork.NeuralNetwork;
import neuralNetwork.NeuralNetworkPackage;

public class Main {

	public static EObject loadModel(String uri) {
		// register XMI resource factory for .xmi extension
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("xmi", new XMIResourceFactoryImpl());
		// create resource set
		ResourceSet resourceSet = new ResourceSetImpl();
		// register FSA metamodel
		resourceSet.getPackageRegistry().put(NeuralNetworkPackage.eINSTANCE.getNsURI(),
		NeuralNetworkPackage.eINSTANCE);
		// create file URI, always use File.getAbsolutePath()
		URI fileUri = URI.createFileURI(new File(uri).getAbsolutePath());
		// load resource
		Resource resource = resourceSet.getResource(fileUri, true);
		// retrieve first EObject in the resource
		return resource.getContents().get(0);
		}
	
	public static void main(String[] args) {
		
		NeuralNetwork neuralNetwork = (NeuralNetwork) loadModel("model/NeuralNetwork.xmi");
		System.out.println(neuralNetwork.getHiddenlayer().size());
	}
}
