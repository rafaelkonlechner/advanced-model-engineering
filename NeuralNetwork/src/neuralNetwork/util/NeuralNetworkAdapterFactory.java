/**
 */
package neuralNetwork.util;

import neuralNetwork.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see neuralNetwork.NeuralNetworkPackage
 * @generated
 */
public class NeuralNetworkAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static NeuralNetworkPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NeuralNetworkAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = NeuralNetworkPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NeuralNetworkSwitch<Adapter> modelSwitch =
		new NeuralNetworkSwitch<Adapter>() {
			@Override
			public Adapter caseNeuralNetwork(NeuralNetwork object) {
				return createNeuralNetworkAdapter();
			}
			@Override
			public Adapter caseDataSet(DataSet object) {
				return createDataSetAdapter();
			}
			@Override
			public Adapter caseDataPoint(DataPoint object) {
				return createDataPointAdapter();
			}
			@Override
			public Adapter casePerceptron(Perceptron object) {
				return createPerceptronAdapter();
			}
			@Override
			public Adapter caseNeuron(Neuron object) {
				return createNeuronAdapter();
			}
			@Override
			public Adapter caseInputLayer(InputLayer object) {
				return createInputLayerAdapter();
			}
			@Override
			public Adapter caseHiddenLayer(HiddenLayer object) {
				return createHiddenLayerAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link neuralNetwork.NeuralNetwork <em>Neural Network</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see neuralNetwork.NeuralNetwork
	 * @generated
	 */
	public Adapter createNeuralNetworkAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link neuralNetwork.DataSet <em>Data Set</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see neuralNetwork.DataSet
	 * @generated
	 */
	public Adapter createDataSetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link neuralNetwork.DataPoint <em>Data Point</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see neuralNetwork.DataPoint
	 * @generated
	 */
	public Adapter createDataPointAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link neuralNetwork.Perceptron <em>Perceptron</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see neuralNetwork.Perceptron
	 * @generated
	 */
	public Adapter createPerceptronAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link neuralNetwork.Neuron <em>Neuron</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see neuralNetwork.Neuron
	 * @generated
	 */
	public Adapter createNeuronAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link neuralNetwork.InputLayer <em>Input Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see neuralNetwork.InputLayer
	 * @generated
	 */
	public Adapter createInputLayerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link neuralNetwork.HiddenLayer <em>Hidden Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see neuralNetwork.HiddenLayer
	 * @generated
	 */
	public Adapter createHiddenLayerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //NeuralNetworkAdapterFactory
